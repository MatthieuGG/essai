__<h1>Bienvenue sur ces quelques pages qui parleront de batterie</h1>__

<p>Site non exhaustif, ne vous fiez pas aux apparences! <p>

<p><a href="https://bitbucket.org/MatthieuGG/essai/wiki/Home" > Vers l'Accueil</a> <p>

<p>Sur ce site (test), vous trouverez:<p>

<h3> Les étapes de la restauration d'une batterie quelconque<h3>
<ol>
<li><a href="https://bitbucket.org/MatthieuGG/essai/wiki/D%C3%A9montage" > Démontage</a></li>
<li><a href="https://bitbucket.org/MatthieuGG/essai/wiki/Nettoyage" > Nettoyage</a></li>
<li><a href="https://bitbucket.org/MatthieuGG/essai/wiki/Protection" > Protection</a>
</ol>

<h3> Ainsi que : </h3>
<ul>
<li><a href="https://bitbucket.org/MatthieuGG/essai/wiki/Liste%20de%20batteurs%20appr%C3%A9ciables" > Portraits de quelques batteurs</a></li>
<li><a href="https://bitbucket.org/MatthieuGG/essai/wiki/MEDIAS" > Lien vers une vidéo Youtube de Staward Copeland (attention à la couleur de sa cymbale ride)</a></li>
<li><a href="https://bitbucket.org/MatthieuGG/essai/wiki/R%C3%A9flexion" > Quelques éléments sur la batterie</a></li>
</ul>


<hr />

```javascript
var oldUnload = window.onbeforeunload;
window.onbeforeunload = function() {
    saveCoverage();
    if (oldUnload) {
        return oldUnload.apply(this, arguments);
    }
};
```

<hr />

<p> (juste un bout de code trouvé dans un tuto pour tester l'intégration, je ne parle pas encore la Java)<p>






<p><a href="mailto:matthieu.gentien@etudiant.univ-brest.fr?subject=Essai d'envoi de courriel&cc=mgentien@gmail.com">Me contacter</a><p>
<p><a href="https://bitbucket.org/MatthieuGG/essai/wiki/Home" > Vers l'Accueil</a> <p>




